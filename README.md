# Flask Minimal

A minimal demo Flask application.

## How to start

    $ cp .env.example .env
    $ make activate
    $ flask initdb
    $ flask shell
    $ flask run --host=0.0.0.0

If you prefer to use [Vagrant](https://www.vagrantup.com):

    1. Install VirtualBox and Vagrant
    2. `vagrant up --provision`
    3. `vagrant ssh`
    4. `cd ~/code`
    5. `make activate`

The [Xshell](https://www.netsarang.com/products/xsh_overview.html) is a good
choice if you are using Windows.
