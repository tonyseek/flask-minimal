VENV_PATH=.venv

.PHONY: help install-deps compile-deps prepare-deps activate

help:
	@echo "commands: help install-deps compile-deps activate"
	@echo "virtualenv: $(PWD)/$(VENV_PATH)"

install-deps: prepare-deps
	"$(VENV_PATH)/bin/pip-sync" requirements.txt requirements-dev.txt

compile-deps: prepare-deps
	"$(VENV_PATH)/bin/pip-compile" --no-index requirements.in
	"$(VENV_PATH)/bin/pip-compile" --no-index requirements-dev.in

prepare-deps:
	@[ -d "$(VENV_PATH)" ] || virtualenv "$(VENV_PATH)"
	"$(VENV_PATH)/bin/pip" install -U pip==8.1.1 setuptools pip-tools

activate:
	@[ -f "$(VENV_PATH)/bin/honcho" ] || $(MAKE) install-deps
	. "$(VENV_PATH)/bin/activate"; honcho run "$${SHELL:-/bin/sh}"; true
