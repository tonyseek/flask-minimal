from __future__ import absolute_import

from werkzeug.contrib.fixers import ProxyFix

from minimal.app import create_app


app = create_app()
app.wsgi_app = ProxyFix(app.wsgi_app)
