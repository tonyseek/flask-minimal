from __future__ import absolute_import

from minimal.app import create_app
from minimal.ext import db


app = create_app()


@app.cli.command()
def initdb():
    """Initialize database."""
    db.engine.echo = True
    db.create_all()
