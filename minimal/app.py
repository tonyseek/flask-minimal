from __future__ import absolute_import

from flask import Flask

from minimal.ext import db
from minimal.note.views import bp as note_bp


def create_app():
    app = Flask(__name__)
    app.config.from_object('envcfg.smart.minimal')

    # extensions
    db.init_app(app)

    # blueprints
    app.register_blueprint(note_bp)

    return app
