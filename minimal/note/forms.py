from __future__ import absolute_import

from flask_wtf import FlaskForm
from wtforms import fields, validators


class NewNoteForm(FlaskForm):
    content = fields.TextAreaField(u'Content', validators=[
        validators.DataRequired(u'Content is required.'),
    ])
    submit = fields.SubmitField(u'Add It')
