from __future__ import absolute_import

import time


def to_timestamp(datetime):
    """Converts a datetime object into a Unix timestamp."""
    timetuple = datetime.timetuple()
    return time.mktime(timetuple)
