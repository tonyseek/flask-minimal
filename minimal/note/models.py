from __future__ import absolute_import

import datetime

from minimal.ext import db


class Note(db.Model):
    id_ = db.Column(db.Integer(), primary_key=True)
    content = db.Column(db.UnicodeText())
    updated_at = db.Column(db.DateTime())
    created_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)

    def make_summary(self, max_length=20):
        if len(self.content) > max_length:
            return u'{0} ...'.format(self.content[:max_length + 1])
        return self.content

    @classmethod
    def get_multi(cls, limit=10, offset=0):
        return db.session.query(cls).limit(limit).offset(offset).all()

    def save(self):
        self.updated_at = datetime.datetime.utcnow()
        db.session.add(self)
        db.session.commit()
