from __future__ import absolute_import

from flask import Blueprint, request, redirect, render_template, url_for, flash

from minimal.note.forms import NewNoteForm
from minimal.note.models import Note
from minimal.note.utils import to_timestamp


bp = Blueprint('note', __name__, url_prefix='/note')


@bp.route('/')
def index():
    offset = request.args.get('offset', type=int, default=0)
    form = NewNoteForm()
    notes = [
        {
            'id': note.id_,
            'summary': note.make_summary(),
            'updated_at': to_timestamp(note.updated_at),
            'created_at': to_timestamp(note.created_at),
        }
        for note in Note.get_multi(limit=20, offset=offset)
    ]
    return render_template('note/index.html', form=form, notes=notes)


@bp.route('/new', methods=['POST'])
def new():
    form = NewNoteForm()
    if form.validate_on_submit():
        note = Note(content=form.content.data)
        note.save()
    else:
        flash(u'error: {0}'.format(form.errors))
    return redirect(url_for('note.index'))
